FROM openjdk:11-jre-slim AS run
COPY target/authService-latest.jar /usr/local/lib/app.jar
ENTRYPOINT ["java","-Xms32m","-Xmx500m","-jar","/usr/local/lib/app.jar"]
