package authService.service;

import com.auth0.jwk.Jwk;
import org.springframework.cache.annotation.Cacheable;

public interface JwtService {
    @Cacheable(value = "jwkCache")
    Jwk getJwk() throws Exception;
}
