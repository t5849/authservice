package authService.service.impl;

import authService.service.JwtService;
import com.auth0.jwk.Jwk;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.security.interfaces.RSAPublicKey;
import java.util.Date;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@SuppressWarnings("unused")
@Service
public class AuthServiceImpl implements authService.service.AuthService {
    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private JwtService jwtService;

    @Value("${keycloak.client-token}")
    private String keycloakClientToken;

    @Value("${keycloak.users}")
    private String keycloakUsers;

    @Value("${keycloak.usersResetPassword}")
    private String keycloakUsersResetPassword;

    @Value("${keycloak.token-uri}")
    private String keycloakTokenUri;

    @Value("${keycloak.user-info-uri}")
    private String keycloakUserInfo;

    @Value("${keycloak.authorization-grant-type}")
    private String grantType;

    @Value("${keycloak.scope}")
    private String scope;

    @Value("${keycloak.client-id}")
    private String clientId;

    @Value("${keycloak.client-secret}")
    private String clientSecret;

    @Override
    public String login(String username, String password) throws Exception {
        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("username", username);
        map.add("password", password);
        map.add("grant_type", grantType);
        map.add("scope", scope);
        map.add("client_id", clientId);
        map.add("client_secret", clientSecret);

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, null);
        String answer = restTemplate.postForObject(keycloakTokenUri, request, String.class);
        assert answer != null;

        Matcher matcher = Pattern.compile("\\{\"access_token\":\"(.+?)\".+").matcher(answer);
        if (matcher.find()) {
            return matcher.group(1);
        }
        throw new Exception("Bad response");
    }

    @Override
    public void register(String username, String password) throws Exception {
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("Authorization", "Bearer " + getClientToken());
        headers.add("Content-Type", "application/json");

        String data = "{\"username\":\"" + username + "\",\"enabled\":\"true\",\"credentials\":" +
                "[{\"type\":\"password\",\"value\":\"" + password + "\",\"temporary\":false}]}'";

        HttpEntity<String> request = new HttpEntity<>(data, headers);
        try {
            restTemplate.postForObject(keycloakUsers, request, String.class);
        } catch (Exception ex) {
            throw new Exception("Bad response");
        }
    }

    @Override
    public void resetPassword(String token, String password) throws Exception {
        DecodedJWT jwt = this.validateJwt(token);
        String sub = jwt.getClaim("sub").asString();

        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("Authorization", "Bearer " + getClientToken());
        headers.add("Content-Type", "application/json");

        String data = "{\"type\":\"password\",\"value\":\"" + password + "\",\"temporary\":false}";

        HttpEntity<String> request = new HttpEntity<>(data, headers);
        try {
            HashMap<String, String> params = new HashMap<>();
            params.put("sub", sub);
            restTemplate.put(keycloakUsersResetPassword, request, params);
        } catch (Exception ex) {
            throw new Exception("Bad response");
        }
    }

    private String getClientToken() {
        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("client_id", "admin-cli");
        map.add("username", "admin");
        map.add("password", "admin");
        map.add("grant_type", "password");

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, null);
        String answer = restTemplate.postForObject(keycloakClientToken, request, String.class);
        assert answer != null;

        Matcher matcher = Pattern.compile("\\{\"access_token\":\"(.+?)\".+").matcher(answer);
        if (matcher.find()) {
            return matcher.group(1);
        }
        return "";
    }

    @Override
    public String validate(String token) throws Exception {
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("Authorization", "Bearer " + token);

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(null, headers);
        try {
            return restTemplate.postForObject(keycloakUserInfo, request, String.class);
        } catch (Exception ex) {
            throw new Exception("Bad response");
        }
    }

    @Override
    public String validate2(String token) throws Exception {
        DecodedJWT jwt = this.validateJwt(token);
        return jwt.getClaim("Username").asString();
    }

    private DecodedJWT validateJwt(String token) throws Exception
    {
        DecodedJWT jwt = JWT.decode(token);

        Jwk jwk = jwtService.getJwk();
        Algorithm algorithm = Algorithm.RSA256((RSAPublicKey) jwk.getPublicKey(), null);
        algorithm.verify(jwt);

        Date expiryDate = jwt.getExpiresAt();
        if (expiryDate.before(new Date()))
            throw new Exception("token is expired");

        this.validate(token);

        return jwt;
    }
}
