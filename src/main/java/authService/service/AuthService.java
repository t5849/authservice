package authService.service;

public interface AuthService {
    String login(String username, String password) throws Exception;
    void register(String username, String password) throws Exception;
    void resetPassword(String token, String password) throws Exception;
    String validate(String token) throws Exception;
    String validate2(String token) throws Exception;
}
