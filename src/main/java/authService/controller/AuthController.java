package authService.controller;

import authService.service.AuthService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@SuppressWarnings("unused")
@RestController
public class AuthController {
    @Autowired
    private AuthService authService;

    @PostMapping(value = "/login", produces = MediaType.APPLICATION_JSON_VALUE)
    public String login(String username, String password) {
        try {
            return authService.login(username, password);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @PostMapping(value = "/register", produces = MediaType.APPLICATION_JSON_VALUE)
    public HashMap<String, String> register(String username, String password) {
        try {
            authService.register(username, password);
            return new HashMap<String, String>() {{
                put("status", "success");
            }};
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @GetMapping("/validate")
    public HashMap<String, String> validate(@RequestHeader("Authorization") String authHeader) {
        try {
            System.out.println(authService.validate(authHeader));
            return new HashMap<String, String>() {{
                put("is_valid", "true");
            }};
        } catch (Exception e) {
            return new HashMap<String, String>() {{
                put("is_valid", "false");
            }};
        }
    }

    @GetMapping("/tests/validate2/{token}")
    public String validate2(@PathVariable String token)
    {
        try
        {
            return authService.validate2(token);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
